all: 
	latexmk -pdf thesis.tex

clean:
	latexmk -C
	rm -f thesis.bbl thesis-blx.bib thesis_diff.pdf thesis.run.xml

preview:
	latexmk -pvc -pdf thesis.tex
