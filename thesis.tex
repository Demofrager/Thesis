%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% texplate.tex
%% UNL thesis document template
%%
%% This work is licensed under the
%% Creative Commons Attribution-NonCommercial 4.0 International License.
%% To view a copy of this license,
%% visit http://creativecommons.org/licenses/by-nc/4.0/.
%%
%% Version 2016/01/07 [3.1.0]
%% Departamento de Informática
%% Faculdade de Ciências e Tecnologia
%% Universidade Nova de Lisboa
%% BUGS and SUGGESTIONS: please submit an issue at the project web page
%%      at: https://github.com/joaomlourenco/unlthesis/
%%
%% HELP: please ask for help at the unlthesis google group at
%%          https://groups.google.com/forum/#!forum/unlthesis
%%      or at the facebook page
%%          https://www.facebook.com/groups/unlthesis/
%%
%% Authors / Contributors:
%%      - João Lourenço <joao.lourenco@fct.unl.pt>
%%      - Bruno Candeias <b.candeias@campus.fct.unl.pt>
%%
%% DONATIONS:
%%     If you think this template really helped you while writing your thesis,
%%     think about doing a small donation. Just access the website
%%     https://github.com/joaomlourenco/unlthesis/
%%     and click in the “Donation” link.
%%     I'll keep a list thanking to all the identified donors that identify
%%     themselves in the “*Add special instructions to the seller:*” box.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The default option marked with (*)
\documentclass[
  doctype=msc,      % phd(*), phdplan, phdprop, msc, mscplan, bsc
  school=unl/fct,       % unl/fct(*), unl/fcsh, unl/ims, ul/ist
  lang=en,              % en(*), fr, it, pt
  coverlang=en,         % defaults to main language
  copyrightlang=en,     % defaults to main language
  fontstyle=kpfonts,    % baskervaldx bookman charter ebgaramond fbb fourier garamond heuristica kpfonts(*) libertine mathpazo1 mathpazo2 newcent newpx newtx
  chapstyle=elegant,    % bianchi bluebox brotherton dash default elegant(*) ell ger hansen ist jenor lyhne madsen pedersen veelo vz14 vz34 vz43
  otherlistsat=front,   % front(*), back
  aftercover=false,     % false=don't true=include the aftercover file (even if exists)
  linkscolor=darkblue,  % darkblue, black (Set to 'black' for PRINTING)
  printcommittee=false, % set to 'false' from submitted versions who should not have the list of committee members
  spine=false,          % (Set to 'true' for PRINTING the book spine)
  biblatex={            % Options for biblatex (see biblatex documentation)
    style=alphabetic,   % numeric(*), alphabetic, authoryear, bwl-FU
    sorting=nyt,        % none, nyt(*), ynt
    firstinits=true,    % render all first and middle names as initials
    sortcites=true,     % If numeric, sort cites by crescent order
    hyperref=true       % Hyperlinks in citations: true(*) false
  },
  memoir={              % See the 'memoir' documentation
    a4paper,            % the paper size/format
    11pt,               % 10pt, 11pt(*), 12pt
    final,              % draft, final  <= Replace 'draft' with 'final' in final version
  },
  media=screen,         % screen(*), paper
  % behavior to be defined in school options based in "\unlthesis@opt@media"
  % definitions for screen: left and right margins are equal, colored links
  % definitions for paper: left and right margins are different, black links
]{unlthesis}

%%============================================================
%%
%%  BEGINNING OF USER CUSTOMIZATION
%%
%%============================================================

%------------------------------------------------------------------
% Additional packages you may want to use (comment those not needed)
%------------------------------------------------------------------

%% VERY IMPORTANT
\usepackage{booktabs}   % Beautiful simple tables
\usepackage{paralist}   % To enable customizable enumerates
\usepackage{soul}       % For pretty inline code and notes
\usepackage{xparse}     % For complex macros
\usepackage{graphicx}   % To import beautiful pictures
\usepackage{mathtools}  % For fancy equations
\usepackage{amsthm}     % For fancy definitions
\usepackage{algpseudocode} % For fancy algorithms with pseudocode
\usepackage{algorithm}  % For the algorithm env to have labels and caption
\usepackage{hyperref}   % For URL reference
\usepackage[final]{pdfpages} % To import pdf files
%% IMPORTANT (consider removing/commenting)
%\usepackage{colortbl} % Use colors in background of table cells

\usepackage[
  textsize=tiny,
  textwidth=70
]{todonotes} % To register todo notes in the text
\setlength{\marginparwidth}{2.5cm}

%------------------------------------------------------------------
% Customization of some packages
%------------------------------------------------------------------

% Where to look for figures
\prependtographicspath{Chapters/Figures}

% Define the Gdl language (using Prolog as the base)
\lstdefinelanguage{gdl}{%
  morekeywords={init, true , does, knows, legal, next, 
    sees, terminal, goal, base, input, percept, not, 
    distinct, role},
  sensitive=true,
  morecomment=[l]{\%},
}

% Setup of listings, for more information check the 'listings' package manual
\lstset{
  language=gdl,
  tabsize=2,
  basicstyle={\footnotesize},
  keywordstyle=\textbf,
  captionpos=b,
  numbers=left,
  numberstyle={\tiny\color{gray}},
  commentstyle={\color{gray}\textit},
  escapechar=\@,
  showstringspaces=false,
  frame=tb,
  rulecolor=\color{lightgray}
}% Inline code

\definecolor{codecolor}{gray}{.9}
\newcommand\code[1]{\sethlcolor{codecolor}\hl{\texttt{#1}}}

% Notes
\definecolor{notecolor}{HTML}{FFE89C}
\DeclareDocumentCommand{\note}{o m}{%
  \sethlcolor{notecolor}\hl{#2}\IfValueTF{#1}{%
    \todo[
      linecolor=notecolor,
      backgroundcolor=notecolor!30,
      bordercolor=notecolor
    ]{#1}
  }{}{}%
}

% Definitions
\newtheorem{mydef}{Definition}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{conjecture}{Conjecture}[section]
%%============================================================
% For customization, see file 'unl-thesis/Schools/*/*/defaults.clo'
% and file 'unl-thesis/lang-text.clo'
% You may override here any of the values defined there e.g.:
% \majorfield[pt]={Engenharia Biomédica}
% \majorfield[en]={Biomedical Engineering}
% \thesiscover[msc]={cover-megi-nova-ims}
% \thesiscover[msc]={cover-mgi-nova-ims}
%%============================================================

% Title of the dissertation/thesis
% Use "\\" to break the title into two or more lines
\title{Epistemic Game Master:\\ A referee for GDL-III Games}
\subtitle{}

% Author
% use [f] for female and [m] for male
\authorname[m]{Filipe Miguel Cristo Sena}{Filipe Sena}
\authordegree{Bachelor of Computer Science and Engineering}

% Date
\datemonth{March}
\dateyear{2018}

% Advisers
% use "\\&" if necessary to break the lines in the appropriate place
% use [f] for female and [m] for male
\coadviser[m]{João Leite}{\\& Associate Professor}{NOVA University Lisbon}
\coadviser[m]{Michael Thielscher}{\\& Professor}{University of New South Wales, Sydney}

% you may use "\newline" to force a new line

% Co-Advisers (maximum of 9)
% use [f] for female and [m] for male
%\coadviser[f]{Name}{Position}{University}

% Committee (maximum of 9 elements)
% Use [p] for chair, [a] for rapporteurs, [v] or nothing for remaining members
% President of the committee
\committee[c,m]{Name of the male committee chairperson}
% Main rapporteurs
\committee[r,f]{Name of a female raporteur}
\committee[r,f]{Name of another (male) raporteur}
% Main rapporteurs
\committee[a,f]{Name of one female adviser}
\committee[a,f]{Name of another female adviser}
% Other members of the committee - include the Adviser in this list
\committee[m,m]{Another member of the committee}
\committee[m,f]{Yet another member of the committee}

%%------------------------------------------------------------
%% All the names inside braces below should correspond to a file
%% with extension ".tex" and located in the "Chapters" folder
%%------------------------------------------------------------

% Dedicatory text. Will only be considered for final documents,
% i.e., "bsc", "msc" and "phd", otherwise, it will be silently ignored
\dedicatoryfile{dedicatory}

% Acknowledgments text. Will only be considered for final documents,
% i.e., "bsc", "msc" and "phd", otherwise, it will be silently ignored
\acknowledgementsfile{acknowledgements}

% \quotefile{quote} % Quote
\abstractfile[pt]{abstract-pt} % Abstract in Portuguese
\abstractfile[en]{abstract-en} % Abstract in English
\glossaryfile{glossary}
\acronymsfile{acronyms}

% Definition of the second cover, to be printed before the copyright (to serve FCSH-UNL)
\aftercoverfile{aftercover}

% The Table of Contents is always printed.
% The other lists below may be commented and omitted.
\addlisttofrontmatter{\listoffigures} % The List of Figures. Comment to omit.
%\addlisttofrontmatter{\listoftables} % The List of Tables. Comment to omit.
%\addlisttofrontmatter{\lstlistoflistings} % The List of Code Listings. Comment to omit.
%\addlisttofrontmatter{\printnoidxglossaries} % The Glossary and List of Acronyms

% Text chapters
% syntax: \chapterfile{file}
\chapterfile{introduction}
\chapterfile{state-of-the-art}
%\chapterfile{proposed-solution}
\chapterfile{the-game-master}
\chapterfile{validation}
\chapterfile{conclusion}

% Text appendixes
% syntax: \appendixfile{file}
\appendixfile{appendex}

% BibTeX bibliography files
% syntax: \addbibfile{file}
\addbibfile{Bibliographies/bibliography.bib}
\addbibfile{Bibliographies/webography.bib}

%% ==== DANGER ==== DANGER ==== DANGER ==== DANGER ==== DANGER ====
%% ==== DANGER ==== DANGER ==== DANGER ==== DANGER ==== DANGER ====
%% ==== DANGER ==== DANGER ==== DANGER ==== DANGER ==== DANGER ====
%%
%% END OF USER CUSTOMIZATION
%% Please do not change below this point!!! :)
%%
%% ==== DANGER ==== DANGER ==== DANGER ==== DANGER ==== DANGER ====
%% ==== DANGER ==== DANGER ==== DANGER ==== DANGER ==== DANGER ====
%% ==== DANGER ==== DANGER ==== DANGER ==== DANGER ==== DANGER ====

% Document
\begin{document}
  \thesisfrontmatter{} % Before the main text (TOC, etc)
  \printcoverpage{} % The cover page
  \printaftercover{}
  \printcopyright{}	% Print the copyright page (will only be printed if adequate for the document type)
  \printdedicatory{} % Print the dedicatory (will only be printed if adequate for the document type)
  \printacknowledgements{} % Print the acknowledgments (will only be printed if adequate for the document type)
  \printquote{}	% Print the quote (if file exists and is adequate for the document type)
  \printabstract{} % Print abstracts (in PT and EN). The abstract in the document main language will be printed first, the abstract in the foreign language will be printed second
  \tableofcontents* % Always print the table of contents
  \printotherlists{} % If option 'otherlistsat=front', print other lists of contents according to instructions given above, otherwise do nothing
  \thesismainmatter{} % The main text
  \printchapthers{} % Print document chapters

  % Print the bibliographies
  \printbib[nottype=online]
  \printbib[title=Webography, type=online]

  %\printotherlists % Alternative location to print the other lists (if you uncomment this one, comment the other one above)
  \printotherlists{} % If option 'otherlistsat=back', print other lists of contents according to instructions given above, otherwise do nothing
  \printappendixes{} % Print appendixes, if any
\end{document}
