#!/bin/bash

# Revisions to compare (by default compares current version of files with latest
# commit)
r1=$1
r2=$2

# Auxiliary folder to create diffed PDF in
tmp=$(mktemp -d tmp_diff_XXXXXX)

# Set -r flags for latexdiff-vc depending on input
if [ -z "$r1" ]; then
  r="-r"
elif [ -z "$r2" ]; then
  r="-r $r1"
else
  r="-r $r1 -r $r2"
fi

# Diff LaTeX files into the temporary folder
latexdiff-vc --git "$r" -d $tmp thesis.tex Chapters/*.tex

# Better diff colours (in r,g,b)
old="0.8,0.3,0.3"
new="0.2,0.6,0.3"

# Change colours for prettier diffs
sed -i "s/\\\\definecolor{RED}{rgb}{1,0,0}\\\\definecolor{BLUE}{rgb}{0,0,1}/\\\\definecolor{old-diff}{rgb}{$old}\\\\definecolor{new-diff}{rgb}{$new}/" $tmp/thesis.tex
sed -i "s/\\\\protect\\\\color{blue}/\\\\protect\\\\color{new-diff}/" $tmp/thesis.tex
sed -i "s/\\\\protect\\\\color{red}/\\\\protect\\\\color{old-diff}/" $tmp/thesis.tex

# Link remaining files needed to compile LaTeX
ln -rs Bibliographies unlthesis-files unlthesis.cls $tmp

# Compile diffed LaTeX, move PDF out of folder and delete folder
cd $tmp
latexmk -pdf thesis.tex
mv thesis.pdf ../thesis_diff.pdf
rm -rf ../$tmp